<footer class="py-5">
    <div class="container py-3">
        <div class="row">
            <div class="col-12 col-lg-4 order-1 order-lg-3 text-center">
                <img src="{{ asset('images/icons/unesco.svg') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
            </div>
            <div class="col-12 col-lg-4 order-2 order-lg-1 pt-5 pt-lg-0">
                <p class="text-white ps-3 m-0">Menu</p>
                <ul class="nav justify-content-between">
                    @foreach ($header as $item)
                        <li class="nav-item w-50">
                            <a class="nav-link text-white pg-lg-0 {{ Route::is($item['route']) ? ' active' : '' }}" href="{{ $item['route'] }}">{{ $item['title'] }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-lg-4 order-3 order-lg-2 pt-5 pt-lg-0">
                <p class="text-white ps-3 m-0">Baixe o nosso app</p>
                <ul class="nav justify-content-between">
                    <li class="nav-item w-50">
                        <a href="#" target="_blank" rel="noopener noreferrer" class="nav-link">
                            <img src="{{ asset('images/icons/app-store.png') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
                        </a>
                    </li>
                    <li class="nav-item w-50">
                        <a href="#" target="_blank" rel="noopener noreferrer" class="nav-link">
                            <img src="{{ asset('images/icons/google-play.png') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row footer pt-3 pt-lg-5">
            <div class="col-12 col-lg-4 order-1 order-lg-3 pt-2 d-lg-flex justify-content-end">
                <ul class="nav d-flex justify-content-around py-2 px-5 p-lg-0">
                    @isset($settings['facebook'])
                        <li class="nav-item">
                            <a target="_blank" rel="noopener noreferrer" href="{{ $settings['facebook'] }}" class="nav-link">
                                <img src="images/icons/facebook.svg" alt="facebook"/>
                            </a>
                        </li>
                    @endisset
                    @isset($settings['instagram'])
                        <li class="nav-item">
                            <a target="_blank" rel="noopener noreferrer" href="{{ $settings['instagram'] }}" class="nav-link">
                                <img src="images/icons/instagram.svg" alt="instagram"/>
                            </a>
                        </li>
                    @endisset
                    {{-- @isset($settings['twitter'])
                        <li class="nav-item">
                            <a target="_blank" rel="noopener noreferrer" href="{{ $settings['twitter'] }}" class="nav-link">
                                <img src="images/icons/twitter.svg" alt="facebook"/>
                            </a>
                        </li>
                    @endisset --}}
                    @isset($settings['youtube'])
                        <li class="nav-item">
                            <a target="_blank" rel="noopener noreferrer" href="{{ $settings['youtube'] }}" class="nav-link">
                                <img src="images/icons/youtube.svg" alt="youtube"/>
                            </a>
                        </li>
                    @endisset
                </ul>
            </div>
            <div class="col-12 col-lg-4 order-2 order-lg-1 pt-3 pt-lg-0 d-lg-flex align-items-center">
                <p class="text-white fw-light text-center text-lg-start m-0">Copyright @ 2021 Centro Educacional PH3 - Todos os direitos reservados.</p>
            </div>
            <div class="col-12 col-lg-4 order-3 order-lg-2 d-flex justify-content-center align-items-center">
                <a href="https://novam3.com.br">
                    <img src="{{ asset('images/icons/novam3.svg') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
                </a>
            </div>
        </div>
    </div>

    <div class="footer-mobile w-100 position-fixed d-md-none">
        <div class="container">
            <ul class="nav justify-content-between footer-mobile-bg rounded-2">
                <li class="nav-item py-2">
                    <a class="nav-link" href="mailto:atendimento@colegioph3.com.br" target="_blank">
                        <img src="{{ asset('images/icons/email_white.svg') }}" alt="email">
                    </a>
                </li>
                <li class="nav-item py-2">
                    <a class="nav-link" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasLogin" aria-controls="offcanvasLogin">
                        <img src="{{ asset('images/icons/login.svg') }}" alt="login">
                    </a>
                </li>
                <li class="nav-item py-2">
                    <a class="nav-link" href="https://api.whatsapp.com/send?phone=558432723550" target="_blank" >
                        <img src="{{ asset('images/icons/whatsapp.svg') }}" alt="Whatsapp">
                    </a>
                </li>
                <li class="nav-item py-2 d-flex justify-content-center align-items-center">
                    <div class="hamburger hamburger--squeeze js-hamburger d-lg-none" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="d-none d-md-block" style="position: fixed; right: 15px; bottom: 90px; width: 75px; z-index: 10">
        <a class="" href="https://api.whatsapp.com/send?phone=558432723550" target="_blank" >
            <img class="img-fluid" src="{{ asset('images/icons/whatsapp-icon8.png') }}" alt="Whatsapp">
        </a>
    </div>

</footer>
