<section class="units py-5">
    <div class="container">
        <div class="col-12">
            <h1 class="about-title text-uppercase">unidades</h1>
            <p class="card-text py-4 about-text">Conheça nossas unidades e venha nos conhecer!</p>
        </div>
        <div class="units-tabs">
            <ul class="nav nav-pills mb-3 flex-nowrap justify-content-between justify-content-lg-start p-lg-3 bg-blue pb-lg-0 px-lg-5 mb-lg-0" id="pills-tab" role="tablist">
                @foreach ($items as $item)
                    <li class="nav-item d-flex align-items-center justify-content-center" role="presentation">
                        <button class="nav-link text-white text-start rounded-0 border-bottom border-4 border-white{{ $loop->first ? ' active' : '' }}" id="pills-{{ $item->id }}-tab" data-bs-toggle="pill" data-bs-target="#pills-{{ $item->id }}" type="button" role="tab" aria-controls="pills-{{ $item->id }}" aria-selected="true">
                                <span>{{ $item->title }}</span>
                        </button>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content bg-blue p-lg-3 px-lg-5" id="pills-tabContent">
                @foreach ($items as $item)
                    <div class="tab-pane fade{{ $loop->first ? ' show active' : '' }}" id="pills-{{ $item->id }}" role="tabpanel" aria-labelledby="pills-{{ $item->id }}-tab">
                        <div class="tab-pane-img position-relative">
                            <div class="col-12 d-flex justify-content-center">
                                <div class="tab-pane-img-bg w-100" style="background-image: url('{{ $item->files->path }}')">
                                    <img src="{{$item->files->path  }}" class="img-fluid d-none" alt="{{ $item->title }}">
                                </div>
                            </div>
                        </div>                        
                        <ul class="nav flex-column flex-lg-row padding">
                            <li class="nav-item nav-option py-2">
                                <ul class="nav flex-nowrap px-3 py-2">
                                    <li class="nav-item d-flex justify-content-center">
                                        <img src="{{ asset('images/icons/localizacao.svg') }}" alt="Localizacao" class="img-fluid "> 
                                    </li>
                                    <li class="nav-item ps-3 text-white fw-light fs-6">
                                        {{ $item->address }}
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-option py-2">
                                <ul class="nav flex-nowrap px-3 py-2">
                                    <li class="nav-item d-flex justify-content-center">
                                        <img src="{{ asset('images/icons/email.svg') }}" alt="email" class="img-fluid"> 
                                    </li>
                                    <li class="nav-item ps-3 text-white fw-light fs-6">
                                        {{ $item->email }}
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item nav-option py-2">
                                <ul class="nav flex-nowrap px-3 py-2">
                                    <li class="nav-item d-flex justify-content-center">
                                        <img src="{{ asset('images/icons/phone.svg') }}" alt="phone" class="img-fluid"> 
                                    </li>
                                    <li class="nav-item ps-3 text-white fw-light fs-6">
                                        {{ $item->phone }}
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="text-center py-5">
                            <a href="{{ $item->link }}" target="_blank" rel="noopener noreferrer" class="btn btn-danger rounded-pill fw-bold px-4">Como Chegar</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>