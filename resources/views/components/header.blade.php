<header>
    <nav class="navbar navbar-expand-md">
        <div class="container justify-content-center">
            <a class="navbar-brand" href="{{ route('index') }}">
                <img src="{{ asset('images/icons/logo.svg') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 w-100 d-flex justify-content-end">
                    <li class="nav-item pe-4">
                        <a class="nav-link text-white btn border border-2 rounded-pill fw-bold" aria-current="page" href="{{ route('tour') }}">Tour Virtual</a>
                    </li>
                    <li class="nav-item pe-5">
                        <a class="nav-link text-white btn border border-0 rounded-pill fw-bold nav-link-login" data-bs-toggle="offcanvas" data-bs-target="#offcanvasLogin" aria-controls="offcanvasLogin">Login</a>
                    </li>
                    <li class="nav-item text-white d-flex justify-content-center align-items-center fw-bold">
                        Menu
                        <div class="hamburger hamburger--squeeze js-hamburger" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
        <div class="offcanvas-body d-flex justify-content-center align-items-center">
            <ul class="nav flex-column">
                @foreach ($header as $item)
                    <li class="nav-item w-100">
                        <a class="nav-link text-white text-start" aria-current="page" href="{{ $item['route'] }}">{{ $item['title'] }}</a>
                    </li>
                @endforeach
                <li class="nav-item pt-5 w-100 d-md-none">
                    <a class="nav-link mt-3 text-white btn px-4 rounded-pill" data-bs-toggle="offcanvas" data-bs-target="#offcanvasLogin" aria-controls="offcanvasLogin">Login</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasLogin" aria-labelledby="offcanvasLoginLabel">
        <div class="offcanvas-header">
            <button type="button" class="btn text-white p-0 d-md-none" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample">
                <img src="{{ asset('images/icons/back.svg') }}" class="pe-3" alt="">Voltar
            </button>
            <button type="button" class="btn text-white p-0 d-none d-md-block" data-bs-toggle="offcanvas" data-bs-target="#offcanvasLogin" aria-controls="offcanvasLogin">
                <img src="{{ asset('images/icons/back.svg') }}" class="pe-3" alt="">Voltar
            </button>
        </div>
        <div class="offcanvas-body d-flex justify-content-center flex-column">
            <h1 class="text-white text-start">Escolha uma unidade</h1>
            <ul class="nav justify-content-center w-100">
                <li class="nav-item">
                    <a class="mt-3 text-white btn rounded-pill" data-bs-toggle="offcanvas" data-bs-target="#offcanvasMatriz" aria-controls="offcanvasMatriz">Matriz</a>
                </li>
                <li class="nav-item">
                    <a class="mt-3 text-white btn rounded-pill" data-bs-toggle="offcanvas" data-bs-target="#offcanvasCidade" aria-controls="offcanvasCidade">Cidade Verde</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasMatriz" aria-labelledby="offcanvasMatrizLabel">
        <div class="offcanvas-header">
            <button type="button" class="btn text-white" data-bs-toggle="offcanvas" data-bs-target="#offcanvasLogin">
                <img src="{{ asset('images/icons/back.svg') }}" class="pe-3" alt="">Voltar
            </button>
        </div>
        <div class="offcanvas-body d-flex justify-content-center flex-column">
            <h1 class="pb-5 text-white fs-1">Matriz</h1>
            <div class="offcanvas-login-options">
                <h1 class="text-white border-bottom border-2 border-danger w-25">Professor</h1>
                <ul class="nav justify-content-center w-100">
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" href="https://siga.activesoft.com.br/login/?instituicao=PH3">Active Soft</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" target="_blank" href="https://classapp.com.br/auth">Agenda digital</a>
                    </li>
                </ul>
            </div>
            <div class="offcanvas-login-options pt-5">
                <h1 class="text-white border-bottom border-2 border-danger w-25">Aluno</h1>
                <ul class="nav justify-content-between w-100">
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" target="_blank" href="https://classapp.com.br/auth">Agenda digital</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" href="https://one.geekie.com.br/">Geekie One</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" href="https://www.santillanaconnect.com/Account/Login/?wtrealm=http%3a%2f%2flms30.uno-internacional.com%2flogin%2funoe%2f&wreply=https%3a%2f%2flms30.uno-internacional.com%2flogin%2fsso%2floginconnect">Uno</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasCidade" aria-labelledby="offcanvasCidadeLabel">
        <div class="offcanvas-header">
            <button type="button" class="btn text-white" data-bs-toggle="offcanvas" data-bs-target="#offcanvasLogin">
                <img src="{{ asset('images/icons/back.svg') }}" class="pe-3" alt="">Voltar
            </button>
        </div>
        <div class="offcanvas-body d-flex justify-content-center flex-column">
            <h1 class="pb-5 text-white fs-1">Cidade Verde</h1>
            <div class="offcanvas-login-options">
                <h1 class="text-white border-bottom border-2 border-danger w-25">Professor</h1>
                <ul class="nav justify-content-center w-100">
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" href="https://siga.activesoft.com.br/login/?instituicao=PH3CV">Active Soft</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" target="_blank" href="https://classapp.com.br/auth">Agenda digital</a>
                    </li>
                </ul>
            </div>
            <div class="offcanvas-login-options pt-5">
                <h1 class="text-white border-bottom border-2 border-danger w-25">Aluno</h1>
                <ul class="nav justify-content-between w-100">
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" target="_blank" href="https://classapp.com.br/auth">Agenda digital</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mt-3 text-white btn px-4 py-2 rounded-pill border-2 bg-transparent border-white" href="https://www.santillanaconnect.com/Account/Login/?wtrealm=http%3a%2f%2flms30.uno-internacional.com%2flogin%2funoe%2f&wreply=https%3a%2f%2flms30.uno-internacional.com%2flogin%2fsso%2floginconnect">Uno</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
