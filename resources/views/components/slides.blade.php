<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
    <div id='dot-slide' class="carousel-indicators">
        @foreach ($items as $key => $item)
             <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $key }}" class="{{ $loop->first ? ' active' : '' }}"
                aria-current="{{ $loop->first ? 'true' : 'false' }}" aria-label="{{ $item->title }}"></button>
        @endforeach
    </div>
    <div class="carousel-inner">
        @foreach ($items as $item)
            <div class="carousel-item {{ $loop->first ? ' active' : '' }} {{ $item->type == "mobile" ? 'd-block d-md-none' : 'd-none d-md-block'}}">
                <a href="{{ $item->link ?? '#' }}" target="_blank" rel="noopener noreferrer">
                    <img src="{{ $item->files->path }}" class="w-100" alt="{{ $item->title }}">
                </a>
            </div>
        @endforeach
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
