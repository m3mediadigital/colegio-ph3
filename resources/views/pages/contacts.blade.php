<x-layout>
    <x-slot name="content">
        <section class="contacts">
            <div class="title py-5" style="background-color: #0563AA">
                <div class="container">
                    <h1 class="text-white pages-title text-uppercase py-3">contato</h1>
                </div>
            </div>
            <div class="row m-0 p-0">
                <div class="col-12 col-lg-6 p-0 m-0 bg-red">
                    <div class="container py-5">
                        {{-- <h2 class="text-white text-center text-lg-start pages-subtitle">Entre em contato</h2> --}}
                        <p class="text-white pt-3 fw-light">
                            Entre em contato conosco. Preencha os campos do formulário que em breve te retornaremos.
                        </p>
                        <x-form action="{{ route('sendContacts') }}" method="post">
                            {{-- @csrf --}}
                            <div class="row">
                                <div class="col-12 pt-3">
                                    <x-input type="text" class="form-control rounded-pill border-0 py-2" placeholder="Nome" name="name" value="{{ old('name') }}"/>
                                    <x-error field="name" class="text-white" />
                                </div>
                                <div class="col-12 col-lg-6 pt-3">
                                    <x-input type="email" class="form-control rounded-pill border-0 py-2" placeholder="E-mail" name="email" value="{{ old('email') }}"/>
                                    <x-error field="email" class="text-white" />
                                </div>
                                <div class="col-12 col-lg-6 pt-3">
                                    <x-input type="tel" class="form-control rounded-pill border-0 py-2 sp_celphones" placeholder="Telefone" name="phone" value="{{ old('phone') }}"/>
                                    <x-error field="phone" class="text-white" />
                                </div>
                                <div class="col-12 col-lg-6 pt-3">
                                    <x-input type="text" class="form-control rounded-pill border-0 py-2" placeholder="Assunto" name="subject" value="{{ old('subject') }}"/>
                                    <x-error field="subject" class="text-white" />
                                </div>
                                <div class="col-12 col-lg-6 pt-3">
                                    <select name="units" class="form-control rounded-pill border-0">
                                        <option value="">Unidade</option>
                                        @foreach ($units as $item)
                                            <option value="{{ $item->title }}">{{ $item->title }}</option>
                                        @endforeach
                                    </select>
                                    <x-error field="units" class="text-white" />
                                </div>
                                <div class="col-12 pt-3">
                                    <x-textarea name="message" placeholder="Mensagem" class="form-control border-0" rows="6">{{ old('message') }}</x-textarea>
                                    <x-error field="message" class="text-white" />
                                </div>
                                <div class="col-12 pt-3 text-center pt-4">
                                    <button type="submit" class="btn bg-blue px-5 py-2 rounded-pill text-white fw-bolder fs-5">Enviar</button>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center flex-column col-12">
                                <div class="row">
                                    <h3 class="text-white pt-4 fw-light text-center">Clique para fazer o tour virtual ou agende uma visita</h3>
                                    <div class="d-flex justify-content-center">
                                        <a class="nav-link bg-blue text-white pt-3 btn rounded-pill fw-bold col-lg-3 col-12" aria-current="page" href="https://maps.app.goo.gl/6wXLT52ZibFfAwHQ7">Tour Virtual</a>                                   
                                    </div>
                                    <div class="nav pt-2 flex-column justify-content-around flex-lg-row padding col-12">
                                        <div class="nav-item nav-option py-2 cols-lg-6">
                                            <p class="text-white pt-5 fw-light text-center">Unidade 1 - Matriz</p>
                                            <ul class="nav flex-nowrap px-3 py-2">
                                                <li class="nav-item d-flex justify-content-center">
                                                    <img src="{{ asset('images/icons/phone2.svg') }}" alt="phone" class="img-fluid"> 
                                                </li>
                                                <li class="nav-item ps-3 text-white fw-light fs-6">
                                                    (84) 98808-3550
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="nav-item nav-option py-2 cols-lg-6">
                                            <p class="text-white pt-5 fw-light text-center">Unidade 2 - Cidade Verde</p>
                                            <ul class="nav flex-nowrap px-3 py-2">
                                                <li class="nav-item d-flex justify-content-center">
                                                    <img src="{{ asset('images/icons/phone2.svg') }}" alt="phone" class="img-fluid"> 
                                                </li>
                                                <li class="nav-item ps-3 text-white fw-light fs-6">
                                                    (84) (84) 98173-2020
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </x-form>
                    </div>
                </div>                    
                <div class="col-12 col-lg-6 p-0 m-0 bg-red">
                    <img src="{{ asset('images/min/menina-contato.png') }}" class="img-fluid w-100" alt="CONTATO"/>
                </div>
            </div>
        </section>
        <x-slot name="js">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
            <script>
                var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                },
                spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };
                $('.sp_celphones').mask(SPMaskBehavior, spOptions);
            </script>
        </x-slot>
    </x-slot>
</x-layout>
