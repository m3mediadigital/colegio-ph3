<x-layout>
    <x-slot name="content">
        <section class="accesslevels pb-5">
            <div class="title py-5 bg-red">
                <div class="container">
                    <h1 class="text-white pages-title text-uppercase col-lg-4 py-3">tour virtual</h1>
                    <nav>
                        <ul class="nav nav-tabs border-0 d-flex justify-content-between" id="nav-tab" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link bg-transparent text-white border-0" data-bs-toggle="tab" data-bs-target="#jr" id="jr-tab" type="button" role="tab" aria-controls="jr" aria-selected="true">PH3 Junior</button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link bg-transparent text-white border-0" data-bs-toggle="tab" data-bs-target="#mtz" id="mtz-tab" type="button" role="tab" aria-controls="mtz" aria-selected="true">PH3 Matriz</button>
                            </li>
                            <li class="nav-item">
                                <button class="nav-link bg-transparent text-white border-0" data-bs-toggle="tab" data-bs-target="#cv" id="cv-tab" type="button" role="tab" aria-controls="cv" aria-selected="true">PH3 Cidade Verde</button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="jr" aria-labelledby="jr-tab" role="tabpanel">
                    <div class="bg-blue w-100"></div>
                    <div class="container">
                        <iframe src="https://www.google.com/maps/embed?pb=!4v1638365177910!6m8!1m7!1sCAoSLEFGMVFpcE1mbklVd1haREdhaW0xcHh4UGpocHd0UXVkdTRxeThfQVQ5cXJr!2m2!1d-5.918097789233151!2d-35.27010172605515!3f185.7024480647533!4f-16.541109611790972!5f0.4000000000000002" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
                <div class="tab-pane fade" id="mtz" aria-labelledby="mtz-tab" role="tabpanel">
                    <div class="bg-blue w-100"></div>
                    <div class="container">
                        <iframe src="https://gothru.co/PhEpT1oKk?index=scene_8&hlookat=111&vlookat=10&fov=140" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
                <div class="tab-pane fade" id="cv" aria-labelledby="cv-tab" role="tabpanel">
                    <div class="bg-blue w-100"></div>
                    <div class="container">
                        <iframe src="https://gothru.co/PeyNhrXUR?index=scene_2&hlookat=107&vlookat=3&fov=140" width="100%" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-layout>
