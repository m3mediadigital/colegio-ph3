<x-layout>
    <x-slot name="content">
        <section class="pages">
            <div class="title py-5 bg-red">
                <div class="container">
                    <h1 class="text-white pages-title text-uppercase py-3">nossa história</h1>
                </div>
            </div>
            <div class="container pt-5">
                @foreach ($items as $item)                
                    <div class="card w-100 border-0">
                        <ul class="m-0">
                            <li class="ps-2">
                                <h3 class="about-title fw-bolder fs-1">{{ $item['title'] }}</h3>
                            </li>
                        </ul>
                        <div class="card-body{{ $loop->last ? ' border-0' : '' }}">
                            <p class="fw-light">
                                {{ $item['content'] }}
                            </p>
                            @isset($item['files'])
                                @if (count($item['files']) === 1)
                                    <img src="{{ asset($item['files'][0]['file']) }}" class="img-fluid w-100 mb-4" alt="{{ $item['title'] }}">
                                @else
                                    <div class="row">
                                        @foreach ($item['files'] as $file)
                                            <div class="col-6 pb-4">
                                                <img src="{{ asset($file['file']) }}" class="img-fluid w-100" alt="{{ $item['title'] }}">
                                            </div>                                            
                                        @endforeach
                                    </div>
                                @endif
                                
                            @endisset

                        </div>
                    </div>
                @endforeach
            </div>
        </section>
        <x-units :items="$units" />
    </x-slot>
</x-layout>