<x-layout>
    <x-slot name="content">
        <x-slides :items="$slides" />
        <section class="about py-5">
            <div class="container">
                <div class="row ">
                    <div class="col-md-8 col-lg-6 d-lg-flex justify-content-center align-items-center flex-column">
                        <h1 class="about-title fw-light">
                            <strong>Bem-vindo</strong> <br/> ao PH3</h1>
                        <p class="about-text py-4 ps-lg-5">Há 30 anos, compartilhamos o mesmo propósito. Existimos para conectar o melhor das pessoas ao futuro que tanto desejam, por isso, nossos resultados vão muito além dos rankings educacionais. </p>
                    </div>
                    <div class="col-md-4 col-lg-6">
                        <img src="{{ asset('images/min/about.png') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
                    </div>
                </div>
            </div>
        </section>
        <section class="education pb-5">
            <div class="container">
                <div class="card-carousel owl-carousel owl-theme">
                    @foreach ($pages->lazy() as $key => $item)
                        <div class="item">
                            {{-- <a href="{{ route('accesslevels') }}" class="text-decoration-none"> --}}
                                <div class="card w-100 border-0">
                                    <div class="card-header py-4 py-lg-2 d-lg-flex align-items-center {{ $key % 2 == 0 ? 'bg-blue' : 'bg-red'}}">
                                        <h1 class="card-text text-white text-capitalize ps-2">{!! $item->title !!}</h1>
                                    </div>
                                    <div class="card-body" style="background-image: url('{{ $item->files->path }}')">
                                        <img src="{{ $item->files->path }}" class="d-none" alt={{ $item->title }}">
                                    </div>
                                </div>
                            {{-- </a> --}}
                        </div>
                    @endforeach   
                </div>
            </div>
        </section>
        <section class="differential bg-red py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1 class="text-white about-title text-capitalize">diferenciais</h1>
                        <p class="card-text py-4 about-text text-white">Para que os sonhos e talentos do seu filho possam crescer seguros, oferecemos uma formação baseada em valores, uma educação cidadã e ferramentas 
                            que facilitam o aprendizado e aproximam o aluno da família e da escola, possibilitando um desenvolvimento integral, na teoria e na prática.</p>
                    </div>
                    <div class="d-lg-none">
                        <div class="card-carousel owl-carousel owl-theme">
                            @foreach ($differentials->lazy() as $item)
                                <div class="item">
                                    <div class="card py-5 rounded-0">
                                        <div class="text-center py-5">
                                            <object type="image/svg+xml" data="{{ $item->files->path }}" class="logo">
                                            {{ $item->title }} <!-- fallback image in CSS -->
                                            </object>
                                        </div>
                                        <div class="card-body pt-3">
                                            <h3 class="text-center card-title">{{ $item->title }}</h3>
                                            <p class="card-text">{{ $item->description }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row d-none d-lg-flex">
                        @foreach ($differentials->lazy() as $item)
                            <div class="col-lg-4 pt-4">
                                <div class="card rounded-0">
                                    <div class="text-center py-4">
                                        <object type="image/svg+xml" data="{{ $item->files->path }}" class="logo pt-lg-5">
                                        {{ $item->title }} <!-- fallback image in CSS -->
                                        </object>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="text-center card-title">{{ $item->title }}</h3>
                                        <p class="card-text">{{ $item->description }}</p>
                                    </div>
                                    
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <x-units :items="$units" />
    </x-slot>
</x-layout>
