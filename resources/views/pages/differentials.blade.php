<x-layout>
    <x-slot name="content">
        <section class="differential pb-5">
            <div class="title py-5" style="background-color: #0563AA">
                <div class="container">
                    <h1 class="text-white pages-title text-uppercase py-3">diferenciais</h1>
                    <p class="text-white">
                        Para que os sonhos e talentos do seu filho possam crescer seguros, oferecemos uma formação baseada em valores, uma educação cidadã e ferramentas 
                        que facilitam o aprendizado e aproximam o aluno da família e da escola, possibilitando um desenvolvimento integral, na teoria e na prática.
                    </p>
                </div>
            </div>
            <div class="container pt-3">
                <div class="row">
                    @foreach ($items->lazy() as $key => $item)
                        <div class="col-12 col-lg-4 pt-3">
                            <div class="card py-5 shadow-none w-100 h-100 border-0 rounded-0 {{ $key % 2 == 0 ? 'bg-red' : 'bg-blue'}}" style="min-height: auto">
                                <div class="text-center py-4">
                                    <object type="image/svg+xml" data="{{ $item->files->path }}" class="logo">
                                    {{ $item->title }} <!-- fallback image in CSS -->
                                    </object>
                                </div>
                                <div class="card-body">
                                    <h3 class="text-center card-title text-white">{{ $item->title }}</h3>
                                    <p class="card-text text-white">{{ $item->description }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </x-slot>
</x-layout>
