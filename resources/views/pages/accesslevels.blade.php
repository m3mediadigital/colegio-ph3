<x-layout>
    <x-slot name="content">
        <section class="accesslevels pb-5">
            <div class="title py-5 bg-red">
                <div class="container">
                    <h1 class="text-white pages-title text-uppercase py-3">níveis de ensino</h1>
                    <nav>
                        <div class="nav nav-tabs border-0 d-flex justify-content-between" id="nav-tab" role="tablist">
                            @foreach ($items->pages()->orderBy("created_at", "asc")->get() as $item)
                                <button class="nav-link bg-transparent text-white border-0 {{ $loop->first ? ' active' : '' }}" id="nav-{{ $item->id }}-tab" data-bs-toggle="tab" data-bs-target="#nav-{{ $item->id }}" type="button" role="tab" aria-controls="nav-{{ $item->id }}" aria-selected="{{ $loop->first ?? false }}">
                                    {{ $item->title }}
                                </button>
                            @endforeach
                        </div>
                    </nav>
                </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
                @foreach ($items->pages()->orderBy("created_at", "asc")->get() as $item)
                    <div class="tab-pane fade {{ $loop->first ? ' show active' : '' }}" id="nav-{{ $item->id }}" role="tabpanel" aria-labelledby="nav-{{ $item->id }}-tab">
                        <div class="bg-blue w-100"></div>
                        <div class="container">
                            <img src="{{ asset($item->files->path) }}" class="img-fluid w-100" alt="{{ $item->title }}">
                            <h1 class="fw-bolder text-uppercase py-4">{{ $item->title }}</h1>
                            {!! $item->description !!}
                            <div class="row">
                                @foreach ($item->contents as $content)
                                    <div class="col-12 pt-4">
                                        <div class="card bg-blue border-top border-danger border-0 border-5 rounded-0 p-3 h-100">
                                            <div class="card-body">
                                                <p class="text text-white fw-bolder">{{ $content->title }}</p>
                                                <div class="text-white">
                                                    {!! $content->description !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </x-slot>
</x-layout>
