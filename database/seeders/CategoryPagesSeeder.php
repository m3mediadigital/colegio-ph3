<?php

namespace Database\Seeders;

use App\Models\CategoryPages;
use Illuminate\Database\Seeder;

class CategoryPagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['title' => 'Níveis de ensino'],
            ['title' => 'Tour Virtual'],
        ];

        foreach($items as $item){
            CategoryPages::create($item);
        }
    }
}
