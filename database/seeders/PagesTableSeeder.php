<?php

namespace Database\Seeders;

use App\Models\CategoryPages;
use App\Models\ContentPages;
use App\Models\Files;
use App\Models\Pages;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Educação infantil',
                'slug' => 'niveis-de-ensino',
                'description' => '<p>Inspirada nos princípios éticos, políticos e estéticos, que orientam nossa prática na perspectiva de formação em valores e na construção de uma visão de mundo, em que sejam criadas as condições para a expressão de sentimentos, opiniões, não somente de si próprio, mas também, do outro, com empatia, criticidade, aprendendo sobre o valor de si, do outro e dos diversos grupos culturais, sendo capaz de questionar e romper com velhos paradigmas.</p><p>A Educação Infantil tem como finalidade desenvolver de forma integral a criança, nos aspectos físico, psicomotor, intelectual, emocional, linguístico, sócio-afetivo e cultural, inspirada nas teorias de Jean Piaget, Vygotsky, Wallon, Reuven Feurstein, Paulo Freire e Yves La Taille, dando todo o embasamento para o trabalho pedagógico e com valores, desenvolvido na escola junto aos alunos.</p><p>Os conteúdos essenciais dentro da Educação Infantil parte do contexto das suas vivências e experiências, procurando desenvolver o interesse da criança pelo processo do conhecimento de si mesmo e do outro; da aquisição da autonomia; da exploração de atividades corporais; da expressão artística em várias linguagens (traços, gestos, danças, mímicas, músicas, sons...); da apropriação da linguagem oral e escrita, através de práticas de estímulo à leitura; e garantir oportunidades à criança de realizar observações, manipular objetos para explorar suas características físicas e estabelecer diversas relações, sejam elas numéricas, sequenciais, entre outros.</p>'

            ],
            [
                'title' => 'Ensino Fundamental I',
                'slug' => 'niveis-de-ensino',
                'description' => '<p>Inspirada nos princípios éticos, políticos e estéticos, que orientam nossa prática na perspectiva de formação em valores e na construção de uma visão de mundo, em que sejam criadas as condições para a expressão de sentimentos, opiniões, não somente de si próprio, mas também, do outro, com empatia, criticidade, aprendendo sobre o valor de si, do outro e dos diversos grupos culturais, sendo capaz de questionar e romper com velhos paradigmas.</p><p>A Educação Infantil tem como finalidade desenvolver de forma integral a criança, nos aspectos físico, psicomotor, intelectual, emocional, linguístico, sócio-afetivo e cultural, inspirada nas teorias de Jean Piaget, Vygotsky, Wallon, Reuven Feurstein, Paulo Freire e Yves La Taille, dando todo o embasamento para o trabalho pedagógico e com valores, desenvolvido na escola junto aos alunos.</p><p>Os conteúdos essenciais dentro da Educação Infantil parte do contexto das suas vivências e experiências, procurando desenvolver o interesse da criança pelo processo do conhecimento de si mesmo e do outro; da aquisição da autonomia; da exploração de atividades corporais; da expressão artística em várias linguagens (traços, gestos, danças, mímicas, músicas, sons...); da apropriação da linguagem oral e escrita, através de práticas de estímulo à leitura; e garantir oportunidades à criança de realizar observações, manipular objetos para explorar suas características físicas e estabelecer diversas relações, sejam elas numéricas, sequenciais, entre outros.</p>'

            ],
            [
                'title' => 'Ensino Fundamental II',
                'slug' => 'niveis-de-ensino',
                'description' => '<p>Inspirada nos princípios éticos, políticos e estéticos, que orientam nossa prática na perspectiva de formação em valores e na construção de uma visão de mundo, em que sejam criadas as condições para a expressão de sentimentos, opiniões, não somente de si próprio, mas também, do outro, com empatia, criticidade, aprendendo sobre o valor de si, do outro e dos diversos grupos culturais, sendo capaz de questionar e romper com velhos paradigmas.</p><p>A Educação Infantil tem como finalidade desenvolver de forma integral a criança, nos aspectos físico, psicomotor, intelectual, emocional, linguístico, sócio-afetivo e cultural, inspirada nas teorias de Jean Piaget, Vygotsky, Wallon, Reuven Feurstein, Paulo Freire e Yves La Taille, dando todo o embasamento para o trabalho pedagógico e com valores, desenvolvido na escola junto aos alunos.</p><p>Os conteúdos essenciais dentro da Educação Infantil parte do contexto das suas vivências e experiências, procurando desenvolver o interesse da criança pelo processo do conhecimento de si mesmo e do outro; da aquisição da autonomia; da exploração de atividades corporais; da expressão artística em várias linguagens (traços, gestos, danças, mímicas, músicas, sons...); da apropriação da linguagem oral e escrita, através de práticas de estímulo à leitura; e garantir oportunidades à criança de realizar observações, manipular objetos para explorar suas características físicas e estabelecer diversas relações, sejam elas numéricas, sequenciais, entre outros.</p>'

            ],
             [
                'title' => 'Ensino Médio',
                'slug' => 'niveis-de-ensino',
                'description' => '<p>Inspirada nos princípios éticos, políticos e estéticos, que orientam nossa prática na perspectiva de formação em valores e na construção de uma visão de mundo, em que sejam criadas as condições para a expressão de sentimentos, opiniões, não somente de si próprio, mas também, do outro, com empatia, criticidade, aprendendo sobre o valor de si, do outro e dos diversos grupos culturais, sendo capaz de questionar e romper com velhos paradigmas.</p><p>A Educação Infantil tem como finalidade desenvolver de forma integral a criança, nos aspectos físico, psicomotor, intelectual, emocional, linguístico, sócio-afetivo e cultural, inspirada nas teorias de Jean Piaget, Vygotsky, Wallon, Reuven Feurstein, Paulo Freire e Yves La Taille, dando todo o embasamento para o trabalho pedagógico e com valores, desenvolvido na escola junto aos alunos.</p><p>Os conteúdos essenciais dentro da Educação Infantil parte do contexto das suas vivências e experiências, procurando desenvolver o interesse da criança pelo processo do conhecimento de si mesmo e do outro; da aquisição da autonomia; da exploração de atividades corporais; da expressão artística em várias linguagens (traços, gestos, danças, mímicas, músicas, sons...); da apropriação da linguagem oral e escrita, através de práticas de estímulo à leitura; e garantir oportunidades à criança de realizar observações, manipular objetos para explorar suas características físicas e estabelecer diversas relações, sejam elas numéricas, sequenciais, entre outros.</p>'

            ],
            [
                'title' => 'Área infantil',
                'slug' => 'tour-virtual',
                'link'=> 'https://google.com'
            ],
            [
                'title' => 'Entrada',
                'slug' => 'tour-virtual',
                'link'=> 'https://google.com'
            ],
            [
                'title' => 'Área comum',
                'slug' => 'tour-virtual',
                'link'=> 'https://google.com'
            ],
            [
                'title' => 'Salas de aula',
                'slug' => 'tour-virtual',
                'link'=> 'https://google.com'
            ],
            [
                'title' => 'Biblioteca',
                'slug' => 'tour-virtual',
                'link'=> 'https://google.com'
            ],
            [
                'title' => 'Pátio',
                'slug' => 'tour-virtual',
                'link'=> 'https://google.com'
            ],
            
        ];

        $contents = [
            [
                'title' => 'Ciranda do Livro',
                'description'=> 'É uma proposta inovadora com intenção de repercutir no sucesso escolar da criança em termos da aquisição da leitura e da escrita nos anos vindouros.'
            ],
            [
                'title' => 'Mascote Literário',
                'description'=> 'Tem como objetivo desenvolver o gosto pela leitura. Além de estimular a participação e o desenvolvimento dos pais na vida escolar do filho, proporcionando um momento prazeroso de leitura entre pais e filhos, na presença do Mascote.'
            ],
            [
                'title' => 'Pequenos Contadores de Histórias',
                'description'=> 'Neste projeto, os alunos com a ajuda da professora terão a oportunidade de trabalhar com diferentes tipos de textos, imagens, para a obtenção de informações sobre como contar histórias e sobre recursos que poderiam utilizar melhorando a comunicação, atenção, iniciativa e, principalmente, a segurança.'
            ],
            [
                'title' => 'Valores',
                'description'=> 'Gerenciar a ansiedade dos alunos em sala de aula por meio dos temas trabalhados, histórias, dinâmicas, debates e músicas; bem como a superação da insegurança e da timidez, reconhecendo como EU estou hoje e nomeando os sentimentos a cada aula desenvolvida.'
            ],
            [
                'title' => 'Mala Surpresa',
                'description'=> 'Nível I e II – tem como objetivo desenvolver a oralidade, a criatividade e o processo de leitura e escrita.'
            ],
        ];

   
        foreach($items as $item){
            $file = new Files;
            $file->name = $item['title'];
            $file->filename = 'images/min/ed-infantil.png';
            $file->path = 'images/min/ed-infantil.png';
            $file->save();

            $page = new Pages;
            $page->files_id = $file->id;
            $page->title = $item['title'];
            $page->description = $item['description'] ?? null;
            $page->link = $item['link'] ?? null;
            $page->category_pages_id = CategoryPages::where('slug', $item['slug'])->first()->id;
            $page->save();    

            foreach($contents as $con){
                $cont = new ContentPages;
                $cont->title = $con['title'];
                $cont->description = $con['description'];
                $cont->pages_id = $page->id;
                $cont->save();   
            }
        }
    }
}
