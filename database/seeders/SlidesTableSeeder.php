<?php

namespace Database\Seeders;

use App\Models\Files;
use App\Models\Slides;
use Illuminate\Database\Seeder;

class SlidesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'files' => 'images/min/slide_mobile.png',
                'title' => 'Banner Mobile',
                'type' => 'mobile',
                'position' => 0,
                'active' => true,
                'start_at' => now(),
                'finish_at' => date('Y-m-d', strtotime('2021-12-30'))
            ],

            [
                'files' => 'images/min/slide_desktop.png',
                'title' => 'Banner Desktop',
                'type' => 'desktop',
                'position' => 0,
                'active' => true,
                'start_at' => now(),
                'finish_at' => date('Y-m-d', strtotime('2025-12-30'))
            ],
        ]; 

        foreach($items as $item){
            $file = new Files;
            $file->name = $item['title'];
            $file->filename = $item['files'];
            $file->path = $item['files'];
            $file->save();

            $slide = new Slides;
            $slide->title = $item['title'];
            $slide->position = $item['position'];
            $slide->active = $item['active'];
            $slide->type = $item['type'];
            $slide->files_id = $file->id;
            $slide->start_at = $item['start_at'];
            $slide->finish_at = $item['finish_at'];
            $slide->save();
        }
    }
}
