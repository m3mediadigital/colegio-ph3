<?php

namespace Database\Seeders;

use App\Models\Differentials;
use App\Models\Files;
use Illuminate\Database\Seeder;

class DifferentialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Construção da cidadania',
                'description' => 'Vários projetos são desenvolvidos para a formação integral e cidadã dos educandos, contribuindo para o desenvolvimento das competências e habilidades necessárias para o século XXI (Cidade do Brincar, PEI, Educação socioemocional, Educação Financeira, Cultura Maker, entre outros).',
                'files' => 'images/icons/cidadania.svg',
            ],
            [
                'title' => 'Sustentabilidade',
                'description' => 'Somos uma escola pertencente ao Programa de Escolas Associadas a UNESCO, pois temos o compromisso de desenvolver projetos que contribuam ativamente para a conquista dos 17 Objetivos de Desenvolvimento Sustentável (ODS), estabelecidos pela ONU e da Agenda da Educação 2030.',
                'files' => 'images/icons/sustentabilidade.svg',
            ],
            [
                'title' => 'Agenda digital',
                'description' => 'Oferece uma comunicação simples e segura para instituições de ensino focadas em melhorar sua comunicação com pais, alunos e colaboradores por meio de smartphones, tablets e computadores.',
                'files' => 'images/icons/agenda.svg',
            ],
            [
                'title' => 'Humanização',
                'description' => 'Consideramos o aluno como ser integral e por isso valorizamos suas dimensões não somente cognitiva, mas também emocional, física e espiritual.',
                'files' => 'images/icons/humanizacao.svg',
            ],
            [
                'title' => 'Projeto potencializar',
                'description' => 'Equipe com formação continuada.',
                'files' => 'images/icons/projeto.svg',
            ],
            [
                'title' => 'Estrutura física',
                'description' => 'Moderna, aconchegante, e com salas de aula adequadas para cada faixa etária.',
                'files' => 'images/icons/fisica.svg',
            ],
        ];

        foreach($items as $item){
            $file = new Files;
            $file->name = $item['title'];
            $file->filename = $item['files'];
            $file->path = $item['files'];
            $file->save();

            $d = new Differentials;
            $d->title = $item['title'];
            $d->description = $item['description'];
            $d->files_id = $file->id;
            $d->save();

        }
    }
}
