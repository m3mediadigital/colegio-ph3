<?php

namespace Database\Seeders;

use App\Models\Files;
use App\Models\Units;
use Illuminate\Database\Seeder;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Unidade 1 - Matriz',
                'address' => 'R. Pedro Bezerra Filho, 35 - Santos Reis, Parnamirim - RN, 59141-175',
                'email' => 'atendimento@colegioph3.com.br',
                'phone' => '(84) 98808-3550',
                'link' => 'https://www.google.com/maps/place/Centro+Educacional+PH3/@-5.8496803,-35.265141,12z/data=!4m9!1m2!2m1!1sph3!3m5!1s0x7b25711db97cff1:0xa660fe19ce23b3ec!8m2!3d-5.918699!4d-35.2702521!15sCgNwaDOSAQZzY2hvb2w?hl=pt-BR'
            ],
            [
                'title' => 'Unidade 2 - Cidade Verde',
                'address' => 'R. Gastão Mariz de Faria, 870 - Nova Parnamirim, Parnamirim - RN, 59152-110',
                'email' => 'cidadeverde@colegioph3.com.br',
                'phone' => '(84) 98173-2020',
                'link' => 'https://www.google.com/maps/place/PH3+Cidade+Verde/@-5.8496803,-35.265141,12z/data=!4m9!1m2!2m1!1sph3!3m5!1s0x7b2f979b0701aed:0x3782c86ebb7ced42!8m2!3d-5.8921568!4d-35.1927241!15sCgNwaDOSARBlZHVjYXRpb25fY2VudGVy'
            ]
        ];

        foreach($items as $item){
            $file = new Files;
            $file->name = $item['title'];
            $file->filename = 'images/min/units.png';
            $file->path = 'images/min/units.png';
            $file->save();

            $u = new Units;
            $u->files_id = $file->id;
            $u->title = $item['title'];
            $u->address = $item['address'];
            $u->email = $item['email'];
            $u->phone = $item['phone'];
            $u->link = $item['link'];
            $u->save();
        }
    }
}
