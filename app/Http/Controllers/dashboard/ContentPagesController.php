<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\ContentPages;
use App\Models\Pages;
use Illuminate\Http\Request;

class ContentPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.content.create',['item' => Pages::find(request()->id)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            ContentPages::create($request->all());
        } catch (\Throwable $th) {
            throw $th;

            return redirect()->back()->with('error', 'Erro ao cadastrar. Por favor tente novamente.');
        }

        // dd($request->all());
        return redirect()->route('dashboard.pages.show', Pages::find($request->pages_id))->with('success', 'Conteúdo cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContentPages  $contentPages
     * @return \Illuminate\Http\Response
     */
    public function show(ContentPages $contentPages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContentPages  $contentPages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.pages.content.edit',['item' => ContentPages::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContentPages  $contentPages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            ContentPages::find($id)->update($request->all());
        } catch (\Throwable $th) {
            throw $th;
            return redirect()->back()->with('error', 'Erro ao atualizar. Por favor tente novamente.');
        }

        // dd($request->all());
        return redirect()->route('dashboard.pages.show', Pages::find($request->pages_id))->with('success', 'Conteúdo atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContentPages  $contentPages
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            ContentPages::find($id)->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Erro ao apagar. Por favor tente novamente.');
        }

        // dd($request->all());
        return redirect()->back()->with('success', 'Conteúdo apagado com sucesso.');
    }
}
