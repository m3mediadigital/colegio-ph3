<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactsRequest;
use App\Mail\Contacts as MailContacts;
use App\Models\CategoryPages;
use App\Models\Contacts;
use App\Models\Differentials;
use App\Models\Pages;
use App\Models\Settings;
use App\Models\Slides;
use App\Models\Units;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    public $settings;

    public function __construct()
    {
        $header = [
            [
                'title' => 'Início',
                'route' => route('index'),
            ],
            // [
            //     'title' => 'Esporte',
            //     'route' => route('index'),
            // ],
            [
                'title' => 'Quem Somos',
                'route' => route('about'),
            ],
            [
                'title' => 'Diferenciais',
                'route' => route('differentials'),
            ],
            [
                'title' => 'Níveis de ensino',
                'route' => route('accesslevels'),
            ],
            [
                'title' => 'Contato',
                'route' => route('contacts'),
            ],
            [
                'title' => 'Tour Virtual',
                'route' => route('tour'),
            ],
        ];

        $this->settings = Settings::select('description','slug')->pluck('description','slug')->all();

        if($this->settings['whatsapp']):
            $numberWhats = preg_replace('/(\D+)/', '', $this->settings['whatsapp']);
            $numberWhats = preg_replace('/ˆ0?(\d{2})9?(\d{8})/', '$1$2', $numberWhats);
            view()->share('whatslink', $numberWhats);
        endif;
        
        view()->share('settings', $this->settings);
        view()->share('header', $header);
    }
    public function index()
    {
        return view('pages.index',[
            'slides' => Slides::where(
                [
                    ['active', 1], ['start_at', '<=', now()->format('Y-m-d')], ['finish_at', '>=', now()->format('Y-m-d')]
                ])->orderBy('position','asc')->get(),
            'pages' => Pages::all(),
            'differentials' => Differentials::all(),
            'units' => $this->getUnits()
        ]);
    }

    public function getUnits()
    {
        return Units::all();
    }

    public function about()
    {
        $items = [
            [
                'title' => '1992',
                'content' => 'O Centro Educacional PH3 nasceu em 1992, fruto de um presente que seu fundador, Senhor Antonio Henrique, deu a sua esposa, a professora Francisca Henrique. A escola nasce com turmas da Educação Infantil, até o 4º ano, e ano após ano foi crescendo em estrutura física, projeto pedagógico e consequentemente, expandindo para turmas do Ensino Fundamental (Anos Finais) e Ensino Médio.',
                'files' => [
                    ['file' => 'images/min/1992.png'],
                ]
            ],
            [
                'title' => '1997',
                'content' => 'Em 1997, inauguramos o prédio do PH3 Junior, com a ampliação de mais 18 salas de aulas, adaptadas para o funcionamento da Educação Infantil e Ensino Fundamental (Anos iniciais). ',
                'files' => [
                    ['file' => 'images/min/1997_1.png'],
                    ['file' => 'images/min/1997_2.png'],
                ]
            ],
            [
                'title' => '1999',
                'content' => 'Em 1999, com o boom da informática, reformamos o prédio da matriz, com a construção de mais 5 salas de aulas e o laboratório de informática.',
            ],
            [
                'title' => '2003',
                'content' => 'O ano de 2003, marcou a transição do Núcleo Educacional Arco-Íris para Centro Educacional PH3.
                A mudança foi necessária pois, em consulta aos alunos adolescentes, eles se referiam a escola como um núcleo infantil. Assim, atendendo a pedidos, o nome da escola foi alterado.',
            ],
            [
                'title' => '2005',
                'content' => 'O prédio administrativo passa por uma reforma, deixando as instalações mais modernas, contemplando a biblioteca escolar “Regina Alves”.',
            ],
            [
                'title' => '2010',
                'content' => 'Rcebemos nas nossas instalações a Faculdade Tales de Mileto, que equipou nossa escola com 3 modernos laboratórios e a ampliação da Biblioteca. Hoje, sem o funcionamento da faculdade, as salas se transformaram no nosso laboratório de ciências e ainda, no laboratório de Robótica.',
            ],
            [
                'title' => '2013',
                'content' => 'O PH3 Júnior recebe uma reforma de ampliação para o funcionamento do Tempo Integral (com dormitório, salas de aulas, refeitório, fraldário) e com a construção de mais 6 salas de aulas e a sala do Ballet.',
                'files' => [
                    ['file' => 'images/min/2013_1.png'],
                    ['file' => 'images/min/2013_2.png'],
                ]
            ],
            [
                'title' => '2019',
                'content' => 'Inauguração da Unidade Cidade Verde.',
            ],
        ];

        return view('pages.about',[
            'items' => $items,
            'units' => $this->getUnits()
        ]);
    }

    public function contacts()
    {
        return view('pages.contacts',[
            'units' => $this->getUnits()
        ]);
    }

    public function accesslevels()
    {
        return view('pages.accesslevels',[
            'items' => CategoryPages::where('slug','niveis-de-ensino')->first()
        ]);
    }

    public function differentials()
    {
        return view('pages.differentials',[
            'items' => Differentials::all()
        ]);
    }

    public function tour()
    {
        return view('pages.tour',[
            // 'items' => CategoryPages::where('slug','tour-virtual')->first()
            'units' => $this->getUnits()
        ]);
    }

    public function sendContacts(ContactsRequest $request)
    {
        $contact = new Contacts;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        $contact->subject = $request->subject;
        $contact->units = $request->units;

        try {
            $contact->save();
        } catch (\Throwable $th) {
            throw $th;
            return redirect()->back()->with('error','Error ao enviar contato. Por favor tente novamente.');
        } finally {

            Mail::to($this->settings['email-contato'])->send(new MailContacts($contact));
        }

        return redirect()->back()->with('success','Em breve entraremos em contato.');        
    }
}
