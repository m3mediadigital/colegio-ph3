<?php

namespace App\Models;


use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ContentPages extends Model
{
    use Sluggable;

    protected $table = "content_pages";
    protected $fillable = [
        'pages_id',
        'title',
        'description'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the pages that owns the ContentPages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pages()
    {
        return $this->belongsTo(Pages::class);
    }
}
