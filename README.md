s## About Project

Base de desenvolvimento contruido com Laravel 8.x, excultado com o Docker.
### Pacotes

- **[Laravel](https://laravel.com)**
- **[Argon Dashboard](https://argon-dashboard-laravel.creative-tim.com/docs/getting-started/installation.html)**
- **[Eloquent UUID](https://github.com/goldspecdigital/laravel-eloquent-uuid)**
- **[Eloquent-Sluggable](https://github.com/cviebrock/eloquent-sluggable)**
- **[Blade UIkit](https://blade-ui-kit.com/)**
- **[Livewire](https://laravel-livewire.com/docs/2.x/quickstart)**
- **[Intervention Image](http://image.intervention.io/getting_started/introduction)**
- **[Estados, Municipios, Bairros e Regiões do Brasil](https://github.com/chandez/Estados-Cidades-IBGE)**
- **[Laravel Minify Html](https://packagist.org/packages/workspace/laravel-minify-html)**


## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [feliipemendonca@outlook.com](mailto:feliipemendonca@outlook.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
